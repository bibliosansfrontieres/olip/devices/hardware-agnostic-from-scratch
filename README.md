## Hardwares at work

### Raspberry pi 4

OS : **Raspberry Pi OS Lite (64-bit)**

IP : **10.10.8.50**

MAC : **e4:5f:01:d5:11:58**

Username : **olip**

Password : **olip**

Specifications :

- Pi Sugar
- Production like (sink, case)

### Lenovo X220

OS : **Ubuntu server 22.04.2**

IP : **10.10.8.208**

MAC : **f0:de:f1:9b:18:ab**

Username : **olip**

Password : **olip**

## Manual installation

### OS installation

#### Raspberry Pi OS Lite (64-bit)

---

#### Ubuntu server 22.04.2 (64-bit)

---

### Update

```
sudo apt update
sudo apt upgrade
sudo reboot
```

### Install dependencies

#### Git

```
sudo apt install git
cd ~
git clone https://gitlab.com/bibliosansfrontieres/olip/devices/hardware-agnostic-from-scratch.git
```

#### Docker

```
curl -fsSL get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker ${USER}
su - ${USER}
```

### Create hotspot

#### Raspberry Pi OS

```
sudo nano /etc/dhcpcd.conf
```

Add those lines at then end of file :

```
interface wlan0
  static ip_address=192.168.10.1
```

Run dnsmasq container :

```
cd ~/hardware-agnostic-from-scratch/rpi/dnsmasq
docker compose up -d
```

Run hostapd container :

```
cd ~/hardware-agnostic-from-scratch/rpi/hostapd
docker compose up -d
```

You should now be able to connect to wifi access point `OLIP-v2-Raspberry`

### Install OLIP v2

```
mkdir ~/olip
cd ~/olip
curl https://gitlab.com/bibliosansfrontieres/olip/olip-api-v2/-/raw/main/.env.example > .docker.env
nano docker-compose.yml
```

Put this definition in `docker-compose.yml` :

```
services:
  api:
    image: alexandrebsf/olip-api-v2:latest
    env_file:
      - .docker.env
    ports:
      - 3000:3000
```

Then run Olip container :

```
docker compose up -d
```

You should now access (when you are connected to access point) to : `http://olip.io:3000`


### Install Traefik

```
mkdir ~/traefik
cd ~/traefik
nano docker-compose.yml
```

Put this definition in `docker-compose.yml` :

```
version: "3.3"

services:

  traefik:
    image: "traefik:v2.10"
    container_name: "traefik"
    command:
      #- "--log.level=DEBUG"
      - "--api.insecure=true"
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--entrypoints.web.address=:80"
    ports:
      - "80:80"
      - "8080:8080"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock:ro"

  whoami:
    image: "traefik/whoami"
    container_name: "simple-service"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.whoami.rule=Host(`whoami.localhost`)"
      - "traefik.http.routers.whoami.entrypoints=web"
```